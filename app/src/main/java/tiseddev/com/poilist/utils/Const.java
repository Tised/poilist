package tiseddev.com.poilist.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
public class Const {

    public static String PHOTO_DIR = Environment.getExternalStorageDirectory() + File.separator + "POIList" + File.separator;
    public static String POI_DETAILS = "POI DETAILS";
}
