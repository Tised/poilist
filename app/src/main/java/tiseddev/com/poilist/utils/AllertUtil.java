package tiseddev.com.poilist.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;

import tiseddev.com.poilist.R;
import tiseddev.com.poilist.interfaces.POIInterface;
import tiseddev.com.poilist.models.POIModel;

/**
 * Created by tised on 7/31/15.
 */
public class AllertUtil {


    private static final String TAG = "ALLERTS";

    public static void addEditPOI(Context context, POIInterface poiInterface, POIModel poiModel) {

        LayoutInflater adbInflater = LayoutInflater.from(context);

        LinearLayout view = (LinearLayout) adbInflater.inflate(R.layout.alert_add_poi, null);

        final EditText poiName = (EditText) view.findViewById(R.id.poi_name);
        final EditText poiDescription = (EditText) view.findViewById(R.id.poi_description);

        //if edit, then fill fields with data
        if (poiModel!=null) {

            poiName.setText(poiModel.getName());
            poiDescription.setText(poiModel.getDescription());
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder
                .setTitle(R.string.add_poi)
                .setView(view)
                .setCancelable(true)
                .setPositiveButton(R.string.yes, (dialog, id) -> {

                    //edit POI here
                    if (poiModel != null) {

                        poiModel.setDescription(poiDescription.getText().toString());
                        poiModel.setName(poiName.getText().toString());
                        poiInterface.editPOI(poiModel);

                    } else {
                        //create new POI here
                        if (!poiName.getText().toString().equals("")) {

                            POIModel newPoiModel = new POIModel(poiName.getText().toString(),
                                    poiDescription.getText().toString());

                            poiInterface.addPOI(newPoiModel);
                        }
                    }
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {

                    dialog.cancel();
                });

        AlertDialog alert11 = alertDialogBuilder.create();
        alert11.show();
    }

    public static void deletePOI(Context context, POIInterface poiInterface, POIModel poiModel){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder
                .setTitle(R.string.delete_poi)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> {

                    poiInterface.deletePOI(poiModel);
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {

                    dialog.cancel();
                });

        AlertDialog alert11 = alertDialogBuilder.create();
        alert11.show();
    }
}
