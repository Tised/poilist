package tiseddev.com.poilist.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


/**
 * Created by tised_000 on 11.06.2015.
 */
public class FragmentUtils {

    /**
     * Method allows you to open any fragment with easy call FragmentUtils.openFragment(...) in every
     * place of your app
     * @param which - fragment to open
     * @param where - Layout where we need to put fragment
     * @param tag - tag for this fragment
     * @param context - God object
     * @param isAddToBackStack - flag add to backstack or not
     */
    public static void openFragment(Fragment which, int where, String tag, Context context, Boolean isAddToBackStack){

        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(tag) == null) {

            FragmentTransaction t = fragmentManager.beginTransaction();

            t.replace(where, which, tag);
            if (isAddToBackStack)
                t.addToBackStack(null);
            t.commit();
        }
    }

    /**
     * Close any fragment by its tag, just call FragmentUtils.closeFragment(...)
     * @param context - God object
     * @param tag - tag of fragment which we need to close
     */
    public static void closeFragment(Context context, String tag) {

        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

        Fragment fragment = fragmentManager.findFragmentByTag(tag);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (fragment != null) {
            transaction.remove(fragment);
            transaction.commitAllowingStateLoss();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            fragment = null;
            popBackStack(context);
        }
    }

    /**
     * Easy pop backstack
     * @param context - God object
     */
    public static void popBackStack(Context context) {

        ((FragmentActivity) context).getSupportFragmentManager().popBackStack();
    }
}
