package tiseddev.com.poilist.utils.DBUtils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import tiseddev.com.poilist.dao.POIModelDAO;
import tiseddev.com.poilist.models.POIModel;

/**
 * Created by tised on 8/12/15.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = "POILIST DATABASE";

    private static String DATABASE_NAME ="poilist.db";

    private static final int DATABASE_VERSION = 1;

    private POIModelDAO poiModelDAO = null;

    Context context;

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource){
        try {

            TableUtils.createTable(connectionSource, POIModel.class);
        }

        catch (SQLException e){

            Log.e(TAG, "error creating DB " + DATABASE_NAME);

            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer){

        switch (oldVer) {
            case 1:
                updateFromVersion1(db, connectionSource, oldVer, newVer);
                break;
            default:
                // no updates needed
                break;
        }
    }

    private void updateFromVersion1(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer) {

        Log.d(TAG, "updating DB from ver 1, adding table alarms ");

        onUpgrade(db, connectionSource, oldVer + 1, newVer);
    }

    public POIModelDAO getPOIModelDAO() throws SQLException {

        if (poiModelDAO == null) {

            poiModelDAO = new POIModelDAO(getConnectionSource(), POIModel.class);
        }

        return poiModelDAO;
    }
}
