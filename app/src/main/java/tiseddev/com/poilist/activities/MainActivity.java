package tiseddev.com.poilist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.sql.SQLException;

import butterknife.Bind;
import butterknife.ButterKnife;
import tiseddev.com.poilist.R;
import tiseddev.com.poilist.adapters.POIAdapter;
import tiseddev.com.poilist.dividers.DividerItemDecoration;
import tiseddev.com.poilist.fragmnets.POIDetailsFragment;
import tiseddev.com.poilist.interfaces.POIInterface;
import tiseddev.com.poilist.interfaces.TitleIteractionListener;
import tiseddev.com.poilist.models.POIModel;
import tiseddev.com.poilist.models.bus_events.ActivityResultEvent;
import tiseddev.com.poilist.utils.AllertUtil;
import tiseddev.com.poilist.utils.Const;
import tiseddev.com.poilist.utils.DBUtils.HelperFactory;
import tiseddev.com.poilist.utils.FragmentUtils;
import tiseddev.com.poilist.utils.bus_utils.ActivityResultBus;

public class MainActivity extends AppCompatActivity implements POIInterface, TitleIteractionListener {

    private static final String TAG = "MAIN ACTIVITY";
    @Bind(R.id.poi_list)
    RecyclerView recyclerView;

    @Bind(R.id.empty_list_hint)
    TextView emptyListHint;

    POIAdapter adapter;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(view -> AllertUtil.addEditPOI(MainActivity.this, MainActivity.this, null));

        adapter = new POIAdapter(this, this);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider_vertical, false, true));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        loadPOIs();
    }

    private void loadPOIs() {

        try {

            Log.d(TAG, "reading all POIs");

            adapter.addItems(HelperFactory.getHelper().getPOIModelDAO().getAllPOIs());

            if (adapter.getItemCount() > 0)
                emptyListHint.setVisibility(View.GONE);
            else
                emptyListHint.setVisibility(View.VISIBLE);
        } catch (SQLException e) {
            Log.e(TAG, "error while read DB == " + Log.getStackTraceString(e));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ActivityResultBus.getInstance().postQueue(
                new ActivityResultEvent(requestCode, resultCode, data));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public POIModel getPOI(int position) {

        return adapter.getItem(position);
    }

    @Override
    public void addPOI(POIModel poiModelToAdd) {

        try {

            Log.d(TAG, "adding new model to DB");

            HelperFactory.getHelper().getPOIModelDAO().create(poiModelToAdd);
            adapter.addItem(poiModelToAdd);

            if (adapter.getItemCount() > 0)
                emptyListHint.setVisibility(RecyclerView.GONE);
        } catch (SQLException e) {
            Log.e(TAG, "error while adding to DB  == " + Log.getStackTraceString(e));
        }
    }

    @Override
    public void editPOI(POIModel poiModelToEdit) {

        try {
            HelperFactory.getHelper().getPOIModelDAO().update(poiModelToEdit);
            adapter.editItem(poiModelToEdit);
        } catch (SQLException e) {
            Log.e(TAG, "error while updating to DB  == " + Log.getStackTraceString(e));
        }
    }

    @Override
    public void deletePOI(POIModel poiModelToDelete) {

        try {

            HelperFactory.getHelper().getPOIModelDAO().delete(poiModelToDelete);

            adapter.deleteItem(poiModelToDelete);

            FragmentUtils.closeFragment(this, Const.POI_DETAILS);

            if (adapter.getItemCount() == 0)
                emptyListHint.setVisibility(RecyclerView.VISIBLE);

        } catch (SQLException e) {
            Log.e(TAG, "error while delete to DB  == " + Log.getStackTraceString(e));
        }
    }

    @Override
    public void showPOI(POIModel poiModelToShow) {

        POIDetailsFragment poiDetailsFragment = POIDetailsFragment.newInstance(poiModelToShow);

        FragmentUtils.openFragment(poiDetailsFragment, R.id.root_frame, Const.POI_DETAILS, this, true);
    }

    @Override
    public void setTitle(String text) {

        toolbar.setTitle(text);
    }
}
