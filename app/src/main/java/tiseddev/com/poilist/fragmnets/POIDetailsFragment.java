package tiseddev.com.poilist.fragmnets;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;
import tiseddev.com.poilist.R;
import tiseddev.com.poilist.animations.HeightAnimation;
import tiseddev.com.poilist.custom_views.MapContainer;
import tiseddev.com.poilist.databinding.DetailsPoiFragmentBinding;
import tiseddev.com.poilist.handlers.PoiDetailsHandlers;
import tiseddev.com.poilist.interfaces.POIDetailsInterface;
import tiseddev.com.poilist.interfaces.POIInterface;
import tiseddev.com.poilist.interfaces.TitleIteractionListener;
import tiseddev.com.poilist.models.POIModel;
import tiseddev.com.poilist.models.bus_events.ActivityResultEvent;
import tiseddev.com.poilist.utils.AllertUtil;
import tiseddev.com.poilist.utils.Const;
import tiseddev.com.poilist.utils.DBUtils.HelperFactory;
import tiseddev.com.poilist.utils.DimensUtil;

public class POIDetailsFragment extends Fragment implements POIDetailsInterface {

    private static final String ARG_POI = "poi";
    private static final String TAG = "POI DETAIL FRAG";
    private static final int PICTURE_REQUEST_CODE = 1111;
    private static final int REQUEST_WRITE_STORAGE = 1;

    private POIModel mPoiModel;

    private TitleIteractionListener mListener;
    private POIInterface poiInterface;

    @Bind(R.id.poi_description_text)
    TextView poiDescriptionText;

    @Bind(R.id.poi_lat)
    EditText poiLat;

    @Bind(R.id.poi_lng)
    EditText poiLng;

    @Bind(R.id.map_container)
    MapContainer mapContainer;

    @Bind(R.id.poi_photo)
    ImageView poiPhoto;

    MapView mMapView;
    int mapSize;
    private Uri outputFileUri;

    public POIDetailsFragment() {
        // Required empty public constructor
    }

    /**
     *
     * @param poiModel Poi model to show.
     * @return A new instance of fragment POIDetailsFragment.
     */
    public static POIDetailsFragment newInstance(POIModel poiModel) {
        POIDetailsFragment fragment = new POIDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_POI, poiModel);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {

            mPoiModel = (POIModel) getArguments().getSerializable(ARG_POI);
        }

        mapSize = (int) (DimensUtil.getScreenHeight(getContext()) * 0.5);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.details_poi_fragment, container, false);

        ButterKnife.bind(this, rootView);
        DetailsPoiFragmentBinding binding = DataBindingUtil.bind(rootView);

        poiDescriptionText.setMovementMethod(new ScrollingMovementMethod());

        binding.setPoiItem(mPoiModel);
        binding.setHandlers(new PoiDetailsHandlers(mPoiModel, this));

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        if (mPoiModel.getFilePath() != null)
            poiPhoto.setImageURI(Uri.parse(mPoiModel.getFilePath()));

        poiPhoto.getLayoutParams().height = mapSize;

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.poi_details_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_delete_poi:

                AllertUtil.deletePOI(getActivity(), poiInterface, mPoiModel);
                break;

            case R.id.action_edit_poi:

                AllertUtil.addEditPOI(getActivity(), poiInterface, mPoiModel);
                break;
        }
        return true;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof TitleIteractionListener) {
            mListener = (TitleIteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TitleIteractionListener");
        }

        if (context instanceof POIInterface) {
            poiInterface = (POIInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement POIInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    @Override
    public void saveCoordinates() {

        try {
            double lat = Double.parseDouble(poiLat.getText().toString());
            double lng = Double.parseDouble(poiLng.getText().toString());

            mPoiModel.setLatitude(lat);
            mPoiModel.setLongtitdue(lng);

            HelperFactory.getHelper().getPOIModelDAO().update(mPoiModel);

            //if map expanded - change marker on map
            if (mMapView.getLayoutParams().height > 0)
                showMarkerOnMap(lat, lng);

            //hiding keyboard
            View view = getActivity().getCurrentFocus();

            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            Snackbar.make(getView(), getString(R.string.coordinates_updated), Snackbar.LENGTH_SHORT).show();
        } catch (NumberFormatException nfe) {

            Snackbar.make(getView(), getString(R.string.bad_coordinates), Snackbar.LENGTH_SHORT).show();
            Log.e(TAG, "error while save coordinates == " + Log.getStackTraceString(nfe));
        } catch (SQLException e) {

            Log.e(TAG, "error while update POI === " + Log.getStackTraceString(e));
        }
    }

    @Override
    public void showOnMap(POIModel poiModel) {

        int currH = mMapView.getLayoutParams().height;
        HeightAnimation heightAnimation = null;

        showMarkerOnMap(poiModel.getLatitude(), poiModel.getLongtitdue());

        if (currH == 0)
            heightAnimation = new HeightAnimation(mMapView, 0, mapSize);
        else
            heightAnimation = new HeightAnimation(mMapView, mapSize, 0);

        heightAnimation.setDuration(500);
        mMapView.startAnimation(heightAnimation);
    }

    @Override
    public void addPhoto(POIModel poiModel) {

        boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                alertDialogBuilder
                        .setTitle(R.string.read_permission)
                        .setMessage(R.string.read_permission_need)
                        .setCancelable(true)
                        .setPositiveButton(R.string.yes, (dialog, id) -> {

                            dialog.dismiss();
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_WRITE_STORAGE);
                        });

                AlertDialog alert11 = alertDialogBuilder.create();
                alert11.show();

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_WRITE_STORAGE);

            }
        } else {

            // Determine Uri of camera image to save.
            final File root = new File(Const.PHOTO_DIR);
            root.mkdirs();

            UUID uuid = UUID.randomUUID();
            final String fname = uuid.toString();
            final File sdImageMainDirectory = new File(root, fname);
            outputFileUri = Uri.fromFile(sdImageMainDirectory);

            // Camera.
            final List<Intent> cameraIntents = new ArrayList<Intent>();
            final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            final PackageManager packageManager = getActivity().getPackageManager();
            final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for (ResolveInfo res : listCam) {
                final String packageName = res.activityInfo.packageName;
                final Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(packageName);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                cameraIntents.add(intent);
            }

            // Filesystem.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

            // Chooser of filesystem options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

            // Add the camera options.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

            startActivityForResult(chooserIntent, PICTURE_REQUEST_CODE);
        }
    }

    private Object mActivityResultSubscriber = new Object() {
        @Subscribe
        public void onActivityResultReceived(ActivityResultEvent event) {
            int requestCode = event.getRequestCode();
            int resultCode = event.getResultCode();
            Intent data = event.getData();
            onActivityResult(requestCode, resultCode, data);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Don't forget to check requestCode before continuing your job
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICTURE_REQUEST_CODE) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Uri selectedImageUri;
                if (isCamera) {
                    selectedImageUri = outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }

                try {
                    mPoiModel.setFilePath(selectedImageUri.toString());
                    HelperFactory.getHelper().getPOIModelDAO().update(mPoiModel);
                    poiPhoto.setImageURI(Uri.parse(mPoiModel.getFilePath()));
                } catch (SQLException e) {
                    Log.e(TAG, "error while update == " + Log.getStackTraceString(e));
                }
            }
        }
    }

    private void showMarkerOnMap(double latitude, double longitude) {

        mMapView.getMapAsync(googleMap -> {

            googleMap.clear();

            // create marker
            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude));

            // Changing marker icon
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

            // adding marker
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude)).zoom(7).build();

            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        });
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), R.string.thank_you, Toast.LENGTH_SHORT).show();
                }
            }
        }

    }
}
