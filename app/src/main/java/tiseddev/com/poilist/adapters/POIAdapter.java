package tiseddev.com.poilist.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import tiseddev.com.poilist.R;
import tiseddev.com.poilist.databinding.ItemPoiBinding;
import tiseddev.com.poilist.handlers.PoiItemHandlers;
import tiseddev.com.poilist.interfaces.POIInterface;
import tiseddev.com.poilist.models.POIModel;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
public class POIAdapter extends RecyclerView.Adapter<POIAdapter.ViewHolder> {

    private static final String TAG = "POI ADAPTER";
    Context context;
    List<POIModel> poiModelList;
    private LayoutInflater layoutInflater;
    POIInterface poiInterface;

    public POIAdapter(Context context, POIInterface poiInterface) {

        this.context = context;
        poiModelList = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        this.poiInterface = poiInterface;
    }

    public void addItem(POIModel poiModelToAdd) {

        poiModelList.add(poiModelToAdd);
        notifyItemInserted(poiModelList.size());
    }

    public void editItem(POIModel poiModelToEdit) {

        int pos = poiModelList.indexOf(poiModelToEdit);

        poiModelList.set(pos, poiModelToEdit);
        notifyItemChanged(pos);
    }

    public void deleteItem(POIModel poiModelToDelete) {

        int pos = poiModelList.indexOf(poiModelToDelete);

        poiModelList.remove(pos);
        notifyItemRemoved(pos);
    }

    public void addItems(List<POIModel> allPOIs) {

        poiModelList.addAll(allPOIs);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemPoiBinding binding;

        public ViewHolder(ItemPoiBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        final ItemPoiBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_poi, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        POIModel currentPoiModel = getItem(position);

        holder.binding.setPoi(currentPoiModel);
        holder.binding.setHandlers(new PoiItemHandlers(currentPoiModel, poiInterface));
    }

    public POIModel getItem(int position) {

        return poiModelList.get(position);
    }

    @Override
    public int getItemCount() {
        return poiModelList.size();
    }
}
