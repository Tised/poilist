package tiseddev.com.poilist;

import android.app.Application;
import android.util.Log;

import tiseddev.com.poilist.utils.DBUtils.HelperFactory;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
public class POILIst extends Application {

    private static final String APP_TAG = "POILIST APP";

    @Override
    public void onCreate(){
        super.onCreate();

        Log.d(APP_TAG, "app started");
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
