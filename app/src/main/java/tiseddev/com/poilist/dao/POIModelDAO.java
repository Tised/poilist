package tiseddev.com.poilist.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import tiseddev.com.poilist.models.POIModel;

/**
 * Created by dmytro_vodnik on 11/21/15.
 */

public class POIModelDAO extends BaseDaoImpl<POIModel, String> {

    private static final String TAG = "RINGTONE DAO";

    public POIModelDAO(ConnectionSource connectionSource,
                            Class<POIModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);

    }

    public List<POIModel> getAllPOIs() throws SQLException {

        return this.queryForAll();
    }
}