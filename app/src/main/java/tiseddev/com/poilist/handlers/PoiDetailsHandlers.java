package tiseddev.com.poilist.handlers;

import android.util.Log;
import android.view.View;

import tiseddev.com.poilist.interfaces.POIDetailsInterface;
import tiseddev.com.poilist.models.POIModel;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
public class PoiDetailsHandlers {

    private static final String TAG = "POI DETAIL HANDLER";

    POIModel poiModel;
    POIDetailsInterface poiDetailsInterface;

    public PoiDetailsHandlers(POIModel poiModel, POIDetailsInterface poiDetailsInterface) {

        this.poiModel = poiModel;
        this.poiDetailsInterface = poiDetailsInterface;
    }

    public void saveCoordinates(View view) {

        Log.d(TAG, "save coordinates clicked");

        poiDetailsInterface.saveCoordinates();
    }

    public void showOnMap(View view) {

        Log.d(TAG, "show POI " + poiModel + "on map clicked");

        poiDetailsInterface.showOnMap(poiModel);
    }

    public void addPhoto(View view) {

        Log.d(TAG, "add photo to POI " + poiModel);

        poiDetailsInterface.addPhoto(poiModel);
    }
}
