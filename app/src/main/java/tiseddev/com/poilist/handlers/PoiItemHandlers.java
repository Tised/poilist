package tiseddev.com.poilist.handlers;

import android.util.Log;
import android.view.View;

import tiseddev.com.poilist.interfaces.POIInterface;
import tiseddev.com.poilist.models.POIModel;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
public class PoiItemHandlers {

    private static final String TAG = "POI ITEM HANDLERS";
    POIModel poiModel;
    POIInterface poiInterface;

    public PoiItemHandlers(POIModel poiModel, POIInterface poiInterface) {

        this.poiModel = poiModel;
        this.poiInterface = poiInterface;
    }

    public void showPoiItem(View view) {

        Log.d(TAG, "show poi called");

        poiInterface.showPOI(poiModel);
    }
}
