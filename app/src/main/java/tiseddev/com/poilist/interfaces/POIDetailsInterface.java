package tiseddev.com.poilist.interfaces;

import tiseddev.com.poilist.models.POIModel;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
public interface POIDetailsInterface {

    void saveCoordinates();
    void showOnMap(POIModel poiModel);
    void addPhoto(POIModel poiModel);
}
