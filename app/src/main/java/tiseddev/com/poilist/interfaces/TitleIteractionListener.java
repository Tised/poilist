package tiseddev.com.poilist.interfaces;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
public interface TitleIteractionListener {

    void setTitle(String text);
}
