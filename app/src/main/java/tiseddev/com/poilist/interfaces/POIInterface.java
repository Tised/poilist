package tiseddev.com.poilist.interfaces;

import tiseddev.com.poilist.models.POIModel;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
public interface POIInterface {

    POIModel getPOI(int position);
    void addPOI(POIModel poiModelToAdd);
    void editPOI(POIModel poiModelToEdit);
    void deletePOI(POIModel poiModelToDelete);
    void showPOI(POIModel poiModelToShow);
}
