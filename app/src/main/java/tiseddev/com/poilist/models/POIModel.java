package tiseddev.com.poilist.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

import tiseddev.com.poilist.BR;

/**
 * Created by dmytro_vodnik on 2/29/16.
 */
@DatabaseTable
public class POIModel extends BaseObservable implements Serializable {

    @DatabaseField(generatedId = true, columnName = "id")
    int id;

    @DatabaseField
    @Bindable
    String name;

    @DatabaseField
    @Bindable
    String description;

    @DatabaseField
    @Bindable
    double latitude;

    @DatabaseField
    @Bindable
    double longtitdue;

    @DatabaseField
    String filePath;

    public POIModel() {}

    public POIModel(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public double getLongtitdue() {
        return longtitdue;
    }

    public void setLongtitdue(double longtitdue) {
        this.longtitdue = longtitdue;
        notifyPropertyChanged(BR.longtitdue);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
        notifyPropertyChanged(BR.latitude);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }

    @Override
    public String toString() {
        return "POIModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", latitude=" + latitude +
                ", longtitdue=" + longtitdue +
                '}';
    }
}
