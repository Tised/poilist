POIList project contains POI (Point of interest) list of user

To add new POI user must press "+" at the main screen.
All items storing into local database

Press on any item opens POI detail fragment, where user can edit name and description of POI

Also, user by typing latitude and longtitude can set POI location, by pressing Save button

Also, user can set picture to POI.